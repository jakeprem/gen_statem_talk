defmodule StateFunctionsSwitch do
  @behaviour :gen_statem

  # Client API
  def start_link do
    StateFunctionsSwitch.start_link([])
  end
  def start_link(opts) do
    :gen_statem.start_link(__MODULE__, [], opts)
  end

  def flip(pid) do
    :gen_statem.cast(pid, :flip)
  end

  def get_count(pid) do
    :gen_statem.call(pid, :get_count)
  end

  # Server Callbacks
  def callback_mode do
    :state_functions
  end

  def init(_args) do
    {:ok, :off, 0, []}
  end

  def off(:cast, :flip, data) do
    {:next_state, :on, data + 1 }
  end
  def off({:call, from}, :get_count, data) do
    {:keep_state_and_data, [{:reply, from, data}]}
  end

  def on(:cast, :flip, data) do
    {:next_state, :off, data}
  end
  def on(event_type, event_content, data) do
    handle_event(event_type, event_content, data)
  end

  def handle_event({:call, from}, :get_count, data) do
    {:keep_state_and_data, [{:reply, from, data}]}
  end
end
