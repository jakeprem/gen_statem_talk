defmodule ServerSwitch do
  use GenServer

  # Client API
  def start_link([]) do
    GenServer.start_link(__MODULE__, [])
  end

  def flip(pid), do: GenServer.cast(pid, :flip)
  def get_count(pid), do: GenServer.call(pid, :get_count)

  # Callbacks
  def init(_opts) do
    data = %{
      count: 0,
      state: :off
    }
    {:ok, data}
  end

  def handle_cast(:flip, %{state: :off, count: count}=data) do
    IO.puts("ON: Switching on. (#{count})")
    {:noreply, %{data | state: :on, count: count+1}}
  end
  def handle_cast(:flip, %{state: :on}=data) do
    IO.puts("OFF: Switching off.")
    {:noreply, %{data | state: :off}}
  end

  def handle_call(:get_count, _from, %{count: count}=data) do
    {:reply, count, data}
  end
end
