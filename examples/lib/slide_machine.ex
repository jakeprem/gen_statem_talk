defmodule SlideMachine do
  @behaviour :gen_statem

  defmodule Deck do
    defstruct title: nil, author: nil, slides: []
  end

  defmodule Slide do
    defstruct title: nil, content: nil
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts]},
      restart: :temporary,
      shutdown: 5000,
      type: :worker
    }
  end

  ################
  ## Client API ##
  ################
  def start_link, do: start_link([])
  def start_link(opts), do: :gen_statem.start_link(__MODULE__, [], opts)

  def next_slide(pid), do: :gen_statem.cast(pid, :next_slide)
  def get_slide(pid), do: :gen_statem.call(pid, :get_slide)
  def get_deck(pid), do: :gen_statem.call(pid, :get_deck)

  ######################
  ## Server Callbacks ##
  ######################
  def callback_mode, do: [:handle_event_function, :state_enter]

  def init(_args) do
    deck = generate_deck()

    {:ok, {:show_deck, deck}, deck, []}
  end

  def handle_event(:enter, _previousState, {state, contents}, _data) do
    IO.puts("Entering state: #{inspect(state)}")
    contents |> inspect() |> IO.puts
    {:keep_state_and_data, []}
  end

  def handle_event(:cast, :next_slide, _state, %{slides: []}=data) do
    IO.puts("No more slides to show")
    {:next_state, :show_end, data}
  end
  def handle_event(:cast, :next_slide, _state, %{slides: [next_slide | rest]}=data) do
    {:next_state, {:show_slide, next_slide}, %{data | slides: rest}}
  end

  def handle_event({:call, from}, :get_slide, {:show_slide, slide}, _data) do
    {:keep_state_and_data, [{:reply, from, slide}]}
  end
  def handle_event({:call, from}, :get_deck, _state, data) do
    {:keep_state_and_data, [{:reply, from, data}]}
  end

  # Catch all
  def handle_event(event_type, event_content, state, data) do
    IO.puts("CATCH ALL")
    {event_type, event_content, state, data} |> inspect() |> IO.puts()
    {:keep_state_and_data, []}
  end

  #######################
  ## Private Functions ##
  #######################
  defp generate_deck do
    slides = [
      %Slide{
        title: "First slide",
        content: "This is the first slide!"
      },
      %Slide{
        title: "Second slide",
        content: "This is the second slide"
      },
      %Slide{

      }
    ]

    deck = %Deck{
      title: "State Machines in Elixir with :gen_statem",
      author: "Jake Prem",
      slides: slides
    }

    deck
  end

end
