defmodule HandleEventSwitch do
  @behaviour :gen_statem

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts]},
      restart: :temporary,
      shutdown: 5000,
      type: :worker
    }
  end

  ################
  ## Client API ##
  ################
  def start_link, do: start_link([])
  def start_link(opts), do: :gen_statem.start_link(__MODULE__, [], opts)

  def flip(pid), do: :gen_statem.cast(pid, :flip)
  def get_count(pid), do: :gen_statem.call(pid, :get_count)

  ######################
  ## Server Callbacks ##
  ######################
  def callback_mode, do: [:handle_event_function, :state_enter]

  def init(_args) do
    {:ok, :off, 0, []}
  end

  def handle_event(:enter, :on, :off, _data) do
    IO.puts("OFF: You just entered the off state!")
    {:keep_state_and_data, []}
  end
  def handle_event(:enter, _oldState, :off, _data) do
    IO.puts("STARTED: You just started the app in the OFF state")
    {:keep_state_and_data, []}
  end
  def handle_event(:cast, :flip, :off, data) do
    {:next_state, :on, data + 1}
  end

  def handle_event(:enter, _oldState, :on, data) do
    IO.puts("ON: You just entered the on state! (#{data})")
    {:keep_state_and_data, []}
  end
  def handle_event(:cast, :flip, :on, data) do
    # A list of actions at the end is optional
    {:next_state, :off, data, []}
  end

  def handle_event({:call, from}, :get_count, _state, data) do
    {:keep_state_and_data, [{:reply, from, data}]}
  end

  # Catch all
  def handle_event(event_type, event_content, state, data) do
    {event_type, event_content, state, data} |> inspect() |> IO.puts()
    {:keep_state_and_data, []}
  end



end
