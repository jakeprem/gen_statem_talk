defmodule PublicState do
  @moduledoc """
  Documentation for PublicState.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PublicState.hello()
      :world

  """
  def hello do
    :world
  end
end
