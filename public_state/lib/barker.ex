defmodule Barker do
  @behaviour :gen_statem

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts]},
      restart: :temporary,
      shutdown: 5000,
      type: :worker,
    }
  end

  ################
  ## Client API ##
  ################
  def start_link(opts) do
    :gen_statem.start_link(__MODULE__, opts, [])
  end



  ######################
  ## Server Callbacks ##
  ######################
  @impl :gen_statem
  def callback_mode do
    [:handle_event_function, :state_enter]
  end

  @impl :gen_statem
  def init(_opts) do
    # {:ok, initial_state, data}
    # {:ok, {:users, users}, {barks: barks}}
    {:ok, {:users, []}, {:barks, []}}
  end

  # Users and Stuff -------------------------
  @impl :gen_statem
  def handle_event(:enter, _, {:users, users}, _data) do
    IO.puts(inspect({:users, users}))
    {:keep_state_and_data, []}
  end
  def handle_event(:cast, {:add_user, new_user}, {:users, users}, data) do
    {:next_state, {:users, [new_user | users]}, data, []}
  end
  def handle_event(:cast, :go_barks, {:users, users}, {:barks, barks}) do
    {:next_state, {:barks, barks}, {:users, users}, []}
  end
  # -----------------------------------------

  # Barks and stuff --------------------------
  def handle_event(:enter, _, {:barks, barks}, _data) do
    IO.puts(inspect({:barks, barks}))
    {:keep_state_and_data, []}
  end
  def handle_event(:cast, {:add_bark, new_bark}, {:barks, barks}, data) do
    {:next_state, {:barks, [new_bark | barks]}, data, []}
  end
  def handle_event(:cast, :go_users, {:barks, barks}, {:users, users}) do
    {:next_state, {:users, users}, {:barks, barks}, []}
  end
  # ------------------------------------------

  def handle_event(:enter, previous_state, state, data) do
    {:enter, previous_state, state, data} |> inspect() |> IO.puts()
    {:keep_state_and_data, []}
  end
  def handle_event(event, event_content, state, data) do
    {event, event_content, state, data} |> inspect() |> IO.puts()
    {:keep_state_and_data, []}
  end

  #######################
  ## Private Functions ##
  #######################
end
